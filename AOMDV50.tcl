
set val(chan)         Channel/WirelessChannel  
set val(prop)         Propagation/TwoRayGround 
set val(ant)          Antenna/OmniAntenna      
set val(ll)           LL                       
set val(ifq)          Queue/DropTail/PriQueue  
set val(ifqlen)       200                      
set val(netif)        Phy/WirelessPhy          
set val(mac)          Mac/802_11               
set val(nn)           50                                     
set val(x)            500
set val(y)            500
set val(energymodel)  EnergyModel
set val(n_ch)         chan_1

set ns [new Simulator]
set f0 [open out02.tr w]
set f1 [open out12.tr w]
set f2 [open out22.tr w]
set f3 [open out32.tr w]


set f4 [open lost02.tr w]
set f5 [open lost12.tr w]
set f6 [open lost22.tr w]
set f7 [open lost32.tr w]

set f8 [open delay02.tr w]
set f9 [open delay12.tr w]
set f10 [open delay22.tr w]
set f11 [open delay32.tr w]

set tracefd [open aomdv_out50.trw]
$ns trace-all $tracefd
$ns use-newtrace
set namtrace [open aomdv_out50.nam w]
$ns namtrace-all-wireless $namtrace $val(x) $val(y)

set topo [new Topography]

$topo load_flatgrid $val(x) $val(y)

create-god $val(nn)

set chan_1 [new $val(chan)]

$ns node-config  -adhocRouting AOMDV \
                 -llType $val(ll) \
                 -macType $val(mac) \
                 -ifqType $val(ifq) \
                 -ifqLen $val(ifqlen) \
                 -antType $val(ant) \
                 -propType $val(prop) \
                 -phyType $val(netif) \
                 -topoInstance $topo \
                 -channel $chan_1   \
               
for {set i 0} {$i < 10} { incr i } {
            set n($i) [$ns node]
            $n($i) random-motion 0  
            $n($i) color red
            $ns at 0.0 "$n($i) color red"
            $ns initial_node_pos $n($i) 15*i+10
    }
for {set j 10} {$j < 20} { incr j } {
            set n($j) [$ns node]
            $n($j) random-motion 0  
            $n($j) color green
            $ns at 0.0 "$n($j) color green"
            $ns initial_node_pos $n($j) 15*j+10
    }
for {set k 20} {$k < 30} { incr k } {
            set n($k) [$ns node]
            $n($k) random-motion 0  
            $n($k) color blue
            $ns at 0.0 "$n($k) color blue"
            $ns initial_node_pos $n($k) 15*k+10
    }

for {set l 30} {$l < 50} { incr l } {
            set n($l) [$ns node]
            $n($l) random-motion 0  
            $n($l) color black
            $ns at 0.0 "$n($l) color black"
            $ns initial_node_pos $n($l) 15*l+10
    }

$ns at 0.0 "$n(0) setdest 91.7 68.0 10000.0"
$ns at 0.5 "$n(1) setdest 28.4 168.3 10000.0"
$ns at 0.7 "$n(2) setdest 27.3 227.4 10000.0"
$ns at 5.0 "$n(3) setdest 20.05 3.98 10000.0"
$ns at 0.0 "$n(4) setdest 30.8 435.3 10000.0"
$ns at 0.0 "$n(5) setdest 166.3 64.0 10000.0"
$ns at 4.3 "$n(6) setdest 182.8 384.2 10000.0"
$ns at 0.0 "$n(7) setdest 104.5 226.2 10000.0"
$ns at 0.0 "$n(8) setdest 135.9 105.3 10000.0"
$ns at 4.0 "$n(9) setdest 104.0 436.3 10000.0"
$ns at 0.0 "$n(10) setdest 251.1 64.8 10000.0"
$ns at 5.0 "$n(11) setdest 251.9 118.3 10000.0"
$ns at 0.0 "$n(12) setdest 173.2 224.6 10000.0"
$ns at 0.0 "$n(13) setdest 249.8 6.7 10000.0"
$ns at 0.5 "$n(14) setdest 242.8 437.0 10000.0"
$ns at 2.0 "$n(15) setdest 413.6 70.3 10000.0"
$ns at 0.0 "$n(16) setdest 204.0 286.0 10000.0"
$ns at 5.0 "$n(17) setdest 418.9 215.4 10000.0"
$ns at 0.0 "$n(18) setdest 110.5 361.3 10000.0"
$ns at 3.0 "$n(19) setdest 175.1 434.9 10000.0"
$ns at 0.0 "$n(20) setdest 249.7 319.0 10000.0"
$ns at 0.0 "$n(21) setdest 248.8 248.1 10000.0"
$ns at 0.0 "$n(22) setdest 344.2 219.1 10000.0"
$ns at 0.0 "$n(23) setdest 360.9 105.5 10000.0"
$ns at 0.0 "$n(24) setdest 473.9 440.3 10000.0"
$ns at 0.0 "$n(25) setdest 102.6 286.2 10000.0"
$ns at 4.0 "$n(26) setdest 483.3 11.7 10000.0"
$ns at 0.0 "$n(27) setdest 484.7 295.1 10000.0"
$ns at 0.0 "$n(28) setdest 485.8 67.9 10000.0"
$ns at 8.0 "$n(29) setdest 486.1 220.7 10000.0"
$ns at 0.0 "$n(30) setdest 191.7 168.0 10000.0"
$ns at 0.5 "$n(31) setdest 282.4 18.3 10000.0"
$ns at 0.7 "$n(32) setdest 27.3 217.4 10000.0"
$ns at 5.0 "$n(33) setdest 20.05 322.98 10000.0"
$ns at 0.0 "$n(34) setdest 30.8 45.3 10000.0"
$ns at 0.0 "$n(35) setdest 166.3 164.0 10000.0"
$ns at 4.3 "$n(36) setdest 182.8 84.2 10000.0"
$ns at 0.0 "$n(37) setdest 104.5 26.2 10000.0"
$ns at 0.0 "$n(38) setdest 135.9 5.3 10000.0"
$ns at 4.0 "$n(39) setdest 104.0 436.3 10000.0"
$ns at 0.0 "$n(40) setdest 91.7 168.0 10000.0"
$ns at 0.5 "$n(41) setdest 281.4 168.3 10000.0"
$ns at 0.7 "$n(42) setdest 271.3 227.4 10000.0"
$ns at 5.0 "$n(43) setdest 202.05 3.98 10000.0"
$ns at 0.0 "$n(44) setdest 302.8 435.3 10000.0"
$ns at 0.0 "$n(45) setdest 66.3 64.0 10000.0"
$ns at 4.3 "$n(46) setdest 82.8 384.2 10000.0"
$ns at 0.0 "$n(47) setdest 14.5 226.2 10000.0"
$ns at 0.0 "$n(48) setdest 35.9 105.3 10000.0"
$ns at 4.0 "$n(49) setdest 10.0 436.3 10000.0"

$ns at 10.3 "$n(8) setdest 91.7 68.0 50.0"
$ns at 10.3 "$n(2) setdest 28.4 168.3 50.0"
$ns at 10.5 "$n(31) setdest 28.4 181.3 10000.0"
$ns at 10.7 "$n(32) setdest 270.3 27.4 10000.0"
$ns at 10.3 "$n(9) setdest 30.8 435.3 50.0"
$ns at 10.3 "$n(0) setdest 166.3 64.0 50.0"
$ns at 10.3 "$n(25) setdest 104.5 226.2 50.0"
$ns at 14.3 "$n(36) setdest 181.8 184.2 10000.0"
$ns at 10.0 "$n(37) setdest 134.5 126.2 10000.0"
$ns at 8.3 "$n(19) setdest 104.0 436.3 50.0"
$ns at 10.0 "$n(30) setdest 11.7 18.0 10000.0"
$ns at 15.0 "$n(33) setdest 200.05 22.98 10000.0"
$ns at 10.0 "$n(38) setdest 13.9 15.3 10000.0"
$ns at 14.0 "$n(39) setdest 124.0 46.3 10000.0"
$ns at 10.0 "$n(40) setdest 191.7 168.0 10000.0"
$ns at 15.0 "$n(43) setdest 402.05 3.98 10000.0"
$ns at 14.3 "$n(46) setdest 182.8 384.2 10000.0"
$ns at 14.0 "$n(49) setdest 110.0 436.3 10000.0"
$ns at 10.3 "$n(5) setdest 251.1 64.8 50.0"
$ns at 10.3 "$n(11) setdest 251.9 118.3 50.0"
$ns at 9.3 "$n(6) setdest 110.5 361.3 50.0"
$ns at 10.3 "$n(14) setdest 175.1 434.9 50.0"
$ns at 10.0 "$n(34) setdest 300.8 450.3 10000.0"
$ns at 10.0 "$n(35) setdest 163.3 167.0 10000.0"
$ns at 13.3 "$n(20) setdest 249.7 319.0 50.0"
$ns at 10.3 "$n(16) setdest 248.8 248.1 50.0"
$ns at 10.5 "$n(41) setdest 481.4 168.3 10000.0"
$ns at 10.7 "$n(42) setdest 171.3 227.4 10000.0"
$ns at 16.3 "$n(15) setdest 360.9 105.5 50.0"
$ns at 10.3 "$n(12) setdest 102.6 286.2 50.0"
$ns at 19.3 "$n(29) setdest 484.7 295.1 50.0"
$ns at 10.3 "$n(26) setdest 485.8 67.9 50.0"
$ns at 11.3 "$n(28) setdest 487.0 131.0 50.0"
$ns at 10.3 "$n(22) setdest 414.9 146.4 50.0"
$ns at 12.3 "$n(1) setdest 26.5 113.7 50.0"
$ns at 10.3 "$n(13) setdest 323.7 8.7 50.0"
$ns at 16.3 "$n(3) setdest 101.2 4.0 50.0"
$ns at 10.3 "$n(23) setdest 330.7 142.1 50.0"
$ns at 10.3 "$n(27) setdest 477.8 374.5 50.0"
$ns at 10.0 "$n(44) setdest 302.8 435.3 10000.0"
$ns at 10.0 "$n(45) setdest 166.3 64.0 10000.0"
$ns at 10.3 "$n(7) setdest 111.5 168.7 50.0"
$ns at 10.3 "$n(17) setdest 419.6 271.8 50.0"
$ns at 18.3 "$n(24) setdest 388.4 438.1 50.0"
$ns at 10.3 "$n(10) setdest 325.5 60.5 50.0"
$ns at 10.0 "$n(47) setdest 144.5 226.2 10000.0"
$ns at 10.0 "$n(48) setdest 135.9 105.3 10000.0"
$ns at 15.3 "$n(4) setdest 29.9 372.5 50.0"
$ns at 13.3 "$n(18) setdest 161.8 318.8 50.0"
$ns at 18.3 "$n(21) setdest 247.6 201.6 50.0"

$ns at 20.0 "$n(30) setdest 194.5 226.2 10000.0"
$ns at 24.0 "$n(40) setdest 15.9 105.3 10000.0"
$ns at 23.8 "$n(5) setdest 91.7 68.0 50.0"
$ns at 25.8 "$n(8) setdest 104.5 226.2 50.0"
$ns at 24.8 "$n(0) setdest 135.9 105.3 50.0"
$ns at 21.8 "$n(14) setdest 104.0 436.3 50.0"
$ns at 23.8 "$n(12) setdest 251.1 64.8 50.0"
$ns at 20.8 "$n(23) setdest 251.9 118.3 50.0"
$ns at 25.8 "$n(25) setdest 173.2 224.6 50.0"
$ns at 18.0 "$n(31) setdest 234.5 216.2 10000.0"
$ns at 25.0 "$n(41) setdest 156.9 15.3 10000.0"
$ns at 25.8 "$n(16) setdest 204.0 286.0 50.0"
$ns at 26.8 "$n(18) setdest 110.5 361.3 50.0"
$ns at 20.8 "$n(20) setdest 248.8 248.1 50.0"
$ns at 21.8 "$n(15) setdest 360.9 105.5 50.0"
$ns at 23.8 "$n(27) setdest 483.3 11.7 50.0"
$ns at 25.8 "$n(29) setdest 484.7 295.1 50.0"
$ns at 27.8 "$n(28) setdest 486.1 220.7 50.0"
$ns at 25.8 "$n(21) setdest 414.9 146.4 50.0"
$ns at 29.8 "$n(2) setdest 26.5 113.7 50.0"
$ns at 27.0 "$n(32) setdest 209.5 126.2 10000.0"
$ns at 25.0 "$n(42) setdest 190.9 165.3 10000.0"
$ns at 23.8 "$n(24) setdest 316.5 437.7 50.0"
$ns at 25.8 "$n(3) setdest 101.2 4.0 50.0"
$ns at 25.8 "$n(1) setdest 23.6 63.1 50.0"
$ns at 21.8 "$n(13) setdest 407.6 9.1 50.0"
$ns at 24.0 "$n(33) setdest 494.5 226.2 100.0"
$ns at 26.0 "$n(43) setdest 315.9 105.3 100.0"
$ns at 23.8 "$n(4) setdest 30.3 293.5 50.0"
$ns at 28.8 "$n(6) setdest 395.3 363.4 50.0"
$ns at 20.8 "$n(29) setdest 477.8 374.5 50.0"
$ns at 29.8 "$n(17) setdest 419.6 271.8 50.0"
$ns at 25.0 "$n(34) setdest 44.5 26.2 100.0"
$ns at 24.0 "$n(44) setdest 157.9 15.3 345.0"
$ns at 28.8 "$n(22) setdest 325.5 60.5 50.0"
$ns at 26.8 "$n(9) setdest 29.9 372.5 50.0"
$ns at 25.0 "$n(35) setdest 104.5 286.2 700.0"
$ns at 26.0 "$n(45) setdest 195.9 165.3 500.0"
$ns at 27.8 "$n(11) setdest 247.6 201.6 50.0"
$ns at 27.0 "$n(36) setdest 494.5 286.2 600.0"
$ns at 28.0 "$n(46) setdest 415.9 205.3 900.0"
$ns at 20.0 "$n(38) setdest 134.5 226.2 70000.0"
$ns at 30.0 "$n(48) setdest 156.9 105.3 7000.0"
$ns at 31.0 "$n(39) setdest 234.5 226.2 400.0"
$ns at 21.0 "$n(49) setdest 123.9 105.3 400.0"
$ns at 20.0 "$n(37) setdest 99.5 299.2 900.0"
$ns at 20.0 "$n(47) setdest 158.9 99.3 9000.0"

$ns at 40.0 "$n(14) setdest 27.3 227.4 5000.0"
$ns at 40.0 "$n(26) setdest 166.3 64.0 750.0"
$ns at 40.0 "$n(19) setdest 182.8 384.2 950.0"
$ns at 40.0 "$n(4) setdest 104.5 226.2 500.0"
$ns at 40.0 "$n(0) setdest 135.9 105.3 500.0"
$ns at 40.0 "$n(24) setdest 249.8 6.7 5000.0"
$ns at 40.0 "$n(2) setdest 242.8 437.0 1000.0"
$ns at 40.0 "$n(10) setdest 413.6 70.3 50.0"
$ns at 40.0 "$n(16) setdest 204.0 286.0 80.0"
$ns at 34.0 "$n(1) setdest 418.9 215.4 100.0"
$ns at 41.0 "$n(23) setdest 110.5 361.3 250.0"
$ns at 40.0 "$n(15) setdest 175.1 434.9 350.0"
$ns at 40.0 "$n(5) setdest 249.7 319.0 450.0"
$ns at 40.0 "$n(8) setdest 473.9 440.3 550.0"
$ns at 40.0 "$n(7) setdest 102.6 286.2 500.0"
$ns at 40.0 "$n(11) setdest 483.3 11.7 501.0"
$ns at 40.0 "$n(20) setdest 484.7 295.1 50.0"
$ns at 46.0 "$n(28) setdest 485.8 67.9 502.0"
$ns at 45.0 "$n(27) setdest 487.0 131.0 502.0"
$ns at 44.0 "$n(29) setdest 414.9 146.4 503.0"
$ns at 40.0 "$n(30) setdest 191.7 168.0 10000.0"
$ns at 40.5 "$n(31) setdest 282.4 18.3 10000.0"
$ns at 40.7 "$n(32) setdest 27.3 217.4 10000.0"
$ns at 45.0 "$n(33) setdest 20.05 322.98 10000.0"
$ns at 40.0 "$n(34) setdest 30.8 45.3 10000.0"
$ns at 40.0 "$n(35) setdest 166.3 164.0 10000.0"
$ns at 44.3 "$n(36) setdest 182.8 84.2 10000.0"
$ns at 40.0 "$n(37) setdest 104.5 26.2 10000.0"
$ns at 40.0 "$n(38) setdest 135.9 5.3 10000.0"
$ns at 44.0 "$n(39) setdest 104.0 436.3 10000.0"
$ns at 40.0 "$n(40) setdest 91.7 168.0 10000.0"
$ns at 40.5 "$n(41) setdest 281.4 168.3 10000.0"
$ns at 40.7 "$n(42) setdest 271.3 227.4 10000.0"
$ns at 45.0 "$n(43) setdest 202.05 3.98 10000.0"
$ns at 40.0 "$n(44) setdest 302.8 435.3 10000.0"
$ns at 40.0 "$n(45) setdest 66.3 64.0 10000.0"
$ns at 44.3 "$n(46) setdest 82.8 384.2 10000.0"
$ns at 40.0 "$n(47) setdest 14.5 226.2 10000.0"
$ns at 40.0 "$n(48) setdest 35.9 105.3 10000.0"
$ns at 44.0 "$n(49) setdest 10.0 436.3 10000.0"
$ns at 43.0 "$n(6) setdest 323.7 8.7 150.0"
$ns at 33.0 "$n(18) setdest 316.5 437.7 560.0"
$ns at 40.0 "$n(21) setdest 243.9 374.2 500.0"
$ns at 41.0 "$n(18) setdest 419.6 271.8 5000.0"
$ns at 42.0 "$n(25) setdest 304.3 281.9 5000.0"
$ns at 43.0 "$n(12) setdest 325.5 60.5 500.0"
$ns at 44.0 "$n(22) setdest 318.5 382.1 500.0"
$ns at 45.0 "$n(17) setdest 29.9 372.5 5000.0"


$ns at 50.0 "$n(0) setdest 91.7 68.0 10000.0"
$ns at 50.5 "$n(1) setdest 28.4 168.3 10000.0"
$ns at 50.7 "$n(2) setdest 27.3 227.4 10000.0"
$ns at 50.0 "$n(3) setdest 20.05 3.98 10000.0"
$ns at 50.0 "$n(4) setdest 30.8 435.3 10000.0"
$ns at 50.0 "$n(5) setdest 166.3 64.0 10000.0"
$ns at 50.3 "$n(6) setdest 182.8 384.2 10000.0"
$ns at 50.0 "$n(7) setdest 104.5 226.2 10000.0"
$ns at 50.0 "$n(8) setdest 135.9 105.3 10000.0"
$ns at 50.0 "$n(9) setdest 104.0 436.3 10000.0"
$ns at 50.0 "$n(10) setdest 251.1 64.8 10000.0"
$ns at 50.0 "$n(11) setdest 251.9 118.3 10000.0"
$ns at 50.0 "$n(12) setdest 173.2 224.6 10000.0"
$ns at 50.0 "$n(13) setdest 249.8 6.7 10000.0"
$ns at 50.5 "$n(14) setdest 242.8 437.0 10000.0"
$ns at 52.0 "$n(15) setdest 413.6 70.3 10000.0"
$ns at 50.0 "$n(16) setdest 304.0 286.0 10000.0"
$ns at 50.0 "$n(30) setdest 191.7 168.0 10000.0"
$ns at 50.5 "$n(31) setdest 282.4 18.3 10000.0"
$ns at 50.7 "$n(32) setdest 127.3 217.4 10000.0"
$ns at 50.0 "$n(33) setdest 120.05 322.98 10000.0"
$ns at 50.0 "$n(34) setdest 130.8 45.3 10000.0"
$ns at 50.0 "$n(35) setdest 266.3 164.0 10000.0"
$ns at 50.3 "$n(36) setdest 282.8 84.2 10000.0"
$ns at 50.0 "$n(37) setdest 204.5 26.2 10000.0"
$ns at 50.0 "$n(38) setdest 435.9 5.3 10000.0"
$ns at 50.0 "$n(39) setdest 304.0 436.3 10000.0"
$ns at 50.0 "$n(40) setdest 291.7 168.0 10000.0"
$ns at 50.5 "$n(41) setdest 181.4 168.3 10000.0"
$ns at 50.7 "$n(42) setdest 171.3 227.4 10000.0"
$ns at 50.0 "$n(43) setdest 102.05 3.98 10000.0"
$ns at 50.0 "$n(44) setdest 102.8 435.3 10000.0"
$ns at 50.0 "$n(45) setdest 266.3 64.0 10000.0"
$ns at 50.3 "$n(46) setdest 282.8 384.2 10000.0"
$ns at 50.0 "$n(47) setdest 140.5 226.2 10000.0"
$ns at 50.0 "$n(48) setdest 350.9 105.3 10000.0"
$ns at 50.0 "$n(49) setdest 100.0 436.3 10000.0"
$ns at 50.0 "$n(17) setdest 418.9 215.4 10000.0"
$ns at 50.0 "$n(18) setdest 110.5 361.3 10000.0"
$ns at 50.0 "$n(19) setdest 175.1 434.9 10000.0"
$ns at 50.0 "$n(20) setdest 249.7 319.0 10000.0"
$ns at 50.0 "$n(21) setdest 248.8 248.1 10000.0"
$ns at 50.0 "$n(22) setdest 344.2 219.1 10000.0"
$ns at 50.0 "$n(23) setdest 360.9 105.5 10000.0"
$ns at 50.0 "$n(24) setdest 473.9 440.3 10000.0"
$ns at 50.0 "$n(25) setdest 102.6 286.2 10000.0"
$ns at 50.0 "$n(26) setdest 483.3 11.7 10000.0"
$ns at 50.0 "$n(27) setdest 484.7 295.1 10000.0"
$ns at 50.0 "$n(28) setdest 485.8 67.9 10000.0"
$ns at 48.0 "$n(29) setdest 486.1 220.7 10000.0"


set udp0 [new Agent/UDP]

$ns attach-agent $n(10) $udp0
set sink0 [new Agent/LossMonitor]
$ns attach-agent $n(1) $sink0
$ns connect $udp0 $sink0

set cbr0 [new Application/Traffic/CBR]
$cbr0 set packetSize_ 512
$cbr0 set rate_ 10kb
$cbr0 set random_ 1

$cbr0 attach-agent $udp0

set udp1 [new Agent/UDP]
$ns attach-agent $n(20) $udp1

set sink1 [new Agent/LossMonitor]
$ns attach-agent $n(4) $sink1

$ns connect $udp1 $sink1


set cbr1 [new Application/Traffic/CBR]
$cbr1 set packetSize_ 512
$cbr1 set rate_ 10kb
$cbr1 set random_ 1
$cbr1 attach-agent $udp1


set udp2 [new Agent/UDP]
$ns attach-agent $n(25) $udp2

set sink2 [new Agent/LossMonitor]
$ns attach-agent $n(16) $sink2


$ns connect $udp2 $sink2


set cbr2 [new Application/Traffic/CBR]
$cbr2 set packetSize_ 512
$cbr2 set rate_ 10kb
$cbr2 set random_ 1
$cbr2 attach-agent $udp2

set udp3 [new Agent/UDP]
$ns attach-agent $n(3) $udp3

set sink3 [new Agent/LossMonitor]
$ns attach-agent $n(6) $sink3

$ns connect $udp3 $sink3

set cbr3 [new Application/Traffic/CBR]
$cbr3 set packetSize_ 512
$cbr3 set rate_ 10kb
$cbr3 set random_ 1
$cbr3 attach-agent $udp3

proc finish {} {
        global ns f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 tracefd namtrace
        close $f0
	close $f1
	close $f2
	close $f3
	close $f4 
        close $f5
        close $f6
        close $f7
        close $f8
        close $f9
        close $f10
        close $f11

 	$ns flush-trace
        close $tracefd
	close $namtrace
	
	exec nam aomdv_out50.nam &
        exec xgraph out02.tr out12.tr out22.tr out32.tr -geometry 800x400 -t Throughput &
	exec xgraph lost02.tr lost12.tr lost22.tr lost32.tr -geometry 800x400 -t Packet_Loss &
	exec xgraph delay02.tr delay12.tr delay22.tr delay32.tr -geometry 800x400 -t Delay &
        exit 0
}

set holdtime0 0
set holdseq0 0

set holdtime1 0
set holdseq1 0

set holdtime2 0
set holdseq2 0

set holdtime3 0
set holdseq3 0

set holdrate0 0
set holdrate1 0
set holdrate2 0
set holdrate3 0

proc record {} {

        global sink0 sink1 sink2 sink3 f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 holdtime0 holdseq0 holdtime1 holdseq1 holdtime2 holdseq2 holdtime3 holdseq3 holdrate0 holdrate1 holdrate2 holdrate3 

       set ns [Simulator instance]
       set time 0.5
       set now [$ns now]
	
	set bw0 [$sink0 set bytes_]
	set bw1 [$sink1 set bytes_]
        set bw2 [$sink2 set bytes_]
        set bw3 [$sink3 set bytes_]
	set bw4 [$sink0 set nlost_]
        set bw5 [$sink1 set nlost_]
        set bw6 [$sink2 set nlost_]
        set bw7 [$sink3 set nlost_]
	set bw8 [$sink0 set lastPktTime_]
        set bw9 [$sink0 set npkts_]
        set bw10 [$sink1 set lastPktTime_]
        set bw11 [$sink1 set npkts_]
        set bw12 [$sink2 set lastPktTime_]
        set bw13 [$sink2 set npkts_]
        set bw14 [$sink3 set lastPktTime_]
        set bw15 [$sink3 set npkts_]

        puts $f0 "$now [expr (($bw0+$holdrate0)*8)/(2*$time)]"
        puts $f1 "$now [expr (($bw1+$holdrate1)*8)/(2*$time)]"
        puts $f2 "$now [expr (($bw2+$holdrate2)*8)/(2*$time)]"
        puts $f3 "$now [expr (($bw3+$holdrate3)*8)/(2*$time)]"

        puts $f4 "$now [expr $bw4/$time]"
        puts $f5 "$now [expr $bw5/$time]"
        puts $f6 "$now [expr $bw6/$time]"
        puts $f7 "$now [expr $bw7/$time]"


        if { $bw9 > $holdseq0 } {
                puts $f8 "$now [expr ($bw8 - $holdtime0)/($bw9 - $holdseq0)]"
        } else {
                puts $f8 "$now [expr ($bw9 - $holdseq0)]"
        }

        if { $bw11 > $holdseq1 } {

                puts $f9 "$now [expr ($bw10 - $holdtime1)/($bw11 - $holdseq1)]"

        } else {

                puts $f9 "$now [expr ($bw11 - $holdseq1)]"

        }

        if { $bw13 > $holdseq2 } {

                puts $f10 "$now [expr ($bw12 - $holdtime2)/($bw13 - $holdseq2)]"

        } else {

                puts $f10 "$now [expr ($bw13 - $holdseq2)]"

        }

        if { $bw15 > $holdseq3 } {

                puts $f11 "$now [expr ($bw14 - $holdtime3)/($bw15 - $holdseq3)]"

        } else {

                puts $f11 "$now [expr ($bw15 - $holdseq3)]"

        }

  
	$sink0 set bytes_ 0
        $sink1 set bytes_ 0
        $sink2 set bytes_ 0
        $sink3 set bytes_ 0
	
	$sink0 set nlost_ 0
        $sink1 set nlost_ 0
        $sink2 set nlost_ 0
        $sink3 set nlost_ 0

        set holdtime0 $bw8
        set holdseq0 $bw9         
	
	set holdrate0 $bw0
        set holdrate1 $bw1
        set holdrate2 $bw2
        set holdrate3 $bw3

        $ns at [expr $now+$time] "record"
}

for {set i 0} {$i < $val(nn) } {incr i} {

    $ns at 60.0 "$n($i) reset";
}

$ns at 0.0 "record"
$ns at 1.0 "$cbr0 start"
$ns at 1.0 "$cbr1 start"
$ns at 1.0 "$cbr2 start"
$ns at 1.0 "$cbr3 start"
$ns at 55.0 "$cbr0 stop"
$ns at 55.0 "$cbr1 stop"
$ns at 55.0 "$cbr2 stop"
$ns at 55.0 "$cbr3 stop"
$ns at 60.0 "finish"

puts "Starting Simulation..."

$ns run

